package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class EmailValidatorTest {
	
	//Extension name should have at least 2 alpha-characters 
	@Test
	public void isValidEmailExtensionRegular() {
		String email="harmeetkang17@gmail.com";
		boolean isValid = EmailValidator.isValidEmail(email);
		assertTrue("The email entered is valid",isValid);
	}
	
	@Test
	public void isValidEmailBoundaryExtensionException() {
		String email="harmeetkang17@gmail.co1";
		boolean isValid = EmailValidator.isValidEmail(email);
		assertFalse("The email entered is not valid",isValid);
	}
	
	@Test
	public void isValidEmailExtensionBoundaryIn() {
		String email="harmeetkang17@gmail.co";
		boolean isValid = EmailValidator.isValidEmail(email);
		assertTrue("The email entered is valid",isValid);
	}
	
	@Test
	public void isValidEmailExtensionBoundaryOut() {
		String email="harmeetkang17@gmail.c";
		boolean isValid = EmailValidator.isValidEmail(email);
		assertFalse("The email entered is not valid",isValid);
	}
	
	//Domain name should have at least 3 alpha-characters in lowercase or numbers
	
	@Test
	public void isValidEmailDomainRegular() {
		String email="harmeetkang17@gma1.com";
		boolean isValid = EmailValidator.isValidEmail(email);
		assertTrue("The email entered is valid",isValid);
	}
	
	@Test
	public void isValidEmailDomainException() {
		String email="harmeetkang17@Gma1.com";
		boolean isValid = EmailValidator.isValidEmail(email);
		assertFalse("The email entered is not valid",isValid);
	}
	
	@Test
	public void isValidEmailDomainBoundaryIn() {
		String email="harmeetkang17@gm1.com";
		boolean isValid = EmailValidator.isValidEmail(email);
		assertTrue("The email entered is valid",isValid);
	}
	
	@Test
	public void isValidEmailDomainBoundaryOut() {
		String email="harmeetkang17@g1.com";
		boolean isValid = EmailValidator.isValidEmail(email);
		assertFalse("The email entered is not valid",isValid);
	}
	
	//Account name should have at least 3 alpha-characters in lowercase (must not start with a number)
	
	@Test
	public void isValidEmailAccountNameRegular() {
		String email="harmeetkang17@gmail.com";
		boolean isValid = EmailValidator.isValidEmail(email);
		assertTrue("The email entered is valid",isValid);
	}
	
	@Test
	public void isValidEmailAccountNameNumberException() {
		String email="1harm@gmail.com";
		boolean isValid = EmailValidator.isValidEmail(email);
		assertFalse("The email entered is not valid",isValid);
	}
	
	@Test
	public void isValidEmailAccountNameUpperCaseException() {
		String email="Harmeet@gmail.com";
		boolean isValid = EmailValidator.isValidEmail(email);
		assertFalse("The email entered is not valid",isValid);
	}
	
	@Test
	public void isValidEmailAccountNameBoundaryIn() {
		String email="har@gmail.com";
		boolean isValid = EmailValidator.isValidEmail(email);
		assertTrue("The email entered is valid",isValid);
	}
	
	@Test
	public void isValidEmailAccountNameBoundaryOut() {
		String email="ha@gmail.com";
		boolean isValid = EmailValidator.isValidEmail(email);
		assertFalse("The email entered is not valid",isValid);
	}
	
	//Email should have one and only one @ symbol.
	
	@Test
	public void isValidEmailSymbolRegular() {
		String email="harmeet@gmail.com";
		boolean isValid = EmailValidator.isValidEmail(email);
		assertTrue("The email entered is valid",isValid);
	}
	
	@Test
	public void isValidEmailSymbolExecption() {
		String email="harmeetgmail.com";
		boolean isValid = EmailValidator.isValidEmail(email);
		assertFalse("The email entered is not valid",isValid);
	}
	
	@Test
	public void isValidEmailSymbolBoundaryOut() {
		String email="harmeet@@gmail.com";
		boolean isValid = EmailValidator.isValidEmail(email);
		assertFalse("The email entered is not valid",isValid);
	}
}
